var express = require('express');
var router = express.Router();
var passport = require('passport');
var multer = require('multer');
var User = require('../models/users');

/* GET home page. */
router.get('/',function(req, res, next) {
  User.find({},function(err,data){
    if(err) throw err;
    res.render('index',{users:data});
  })
});
//upload image
function middleware(req, res, next) {
  var imageName;
  var uploadStorage = multer.diskStorage({
      destination: function (req, file, cb) {
          cb(null, 'public/uploads/');
      },
      filename: function (req, file, cb) {
          imageName = file.originalname;
          cb(null, imageName);
      }
  });
  var upload = multer({storage: uploadStorage});
  var uploadFile = upload.single('image');
  uploadFile(req, res, function (err) {
      req.imageName = imageName;
      req.uploadError = err;
      next();
  })
}

//sign up
router.get('/signup',function(req,res,next){
  var messages = req.flash('error');
  res.render('user/signup',{messages:messages,hasErrors:messages.length > 0});
});
router.post('/signup', middleware ,passport.authenticate('local.signup', {
  successRedirect: '/',
  failureRedirect: '/signup',
  failureFlash: true
}));

//sign in
router.get('/signin', function (req, res, next) {
  var messages = req.flash('error');
  res.render('user/signin', {messages: messages, hasErrors: messages.length > 0});
});
router.post('/signin', passport.authenticate('local.signin', {
    successRedirect: '/',
    failureRedirect: '/signin',
    failureFlash: true
}));

//logout
router.get('/logout', function (req, res, next) {
  req.logout();
  res.redirect('/');
});
//edit page
router.get('/edit/:id',function(req,res){
  User.findById(req.params.id, (err,data) => {
      if (err) {
          throw err;
      } else {
        res.render('edit-info',{user:data});
      }
      console.log(data);
  });
});
//update
router.post('/user/edit/:id', middleware , (req, res) => {
  var newUser = new User();
  var user = {
      username:req.body.username,
      email:req.body.email,
      phone:req.body.phone,
      password:newUser.encryptPassword(req.body.newpassword),
      userimage:req.imageName
  };
  var query = {_id:req.params.id};
  User.update(query,user , (err) => {
    if (err) {
      console.log(err);
      return;
    } else {
        res.redirect('/');
    }
  });
});
//delete
router.delete('/delete/:id', (req, res) => {
  var query = {_id:req.params.id};
  User.remove(query, (err) => {
    if (err) {
      console.log(err);
    }
    res.send('Success');
  });
});

module.exports = router;