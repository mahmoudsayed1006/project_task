var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
mongoose.connect("mongodb://task2018:task2018@ds131983.mlab.com:31983/task");
const Schema = mongoose.Schema;
const userSchema = new Schema({
    username:{type:String,require:true},
    email:{type:String,require:true},
    phone:{type:String,require:true},
    password:{type: String, require: true},
    userimage:{type: String},
});
userSchema.methods.encryptPassword = function(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
};

userSchema.methods.validPassword = function(password) { 
  return bcrypt.compareSync(password, this.password);
};
const user = mongoose.model('User', userSchema);

module.exports = user ;
