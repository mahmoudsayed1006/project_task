$(document).ready(function(){
    $('.delete-user').on('click', function(){
      const id = $(this).attr("data-id");
      $.ajax({
        type:'DELETE',
        url: '/delete/'+id,
        success: function(response){
          alert('Deleting user!');
          window.location.href='/';
        },
        error: function(err){
          console.log(err);
        }
      });
    });
  });