var passport = require('passport');
var User = require('../models/users');
var LocalStrategy = require('passport-local').Strategy;
passport.serializeUser(function (user, done) {
    done(null, user.id);
});
passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});
passport.use('local.signup',new LocalStrategy({
  usernameField:'phone',
  passwordField: 'password',
  passReqToCallback: true
}, function(req, phone, password , done){
      //req.checkBody('email','Invalid email try again').notEmpty().isEmail();
      //req.checkBody('username','Invalid name try again').notEmpty();
      req.checkBody('phone', 'Invalid phone').notEmpty();
      req.checkBody('password', 'Password is invalid').notEmpty().isLength({min: 4});
      var errors = req.validationErrors();
      if (errors) {
          var messages = [];
          errors.forEach(function(error) {
             messages.push(error.msg);
          });
          return done(null, false, req.flash('error', messages));
      }
      User.findOne({'phone':phone},function(err,user){
            if(err){
                return done(err);
            }
            if(user){
                return done(null, false, {message: 'Number is already in use.'});
            }
            var newUser = new User();
            newUser.username = req.body.username;
            newUser.email = req.body.email;
            newUser.phone = phone;
            newUser.password = newUser.encryptPassword(password);
            newUser.userimage = req.imageName;
            newUser.save(function(err, result) {
             if (err) {
                 return done(err);
             }
             return done(null, newUser);
          });
        });
}));

 passport.use('local.signin', new LocalStrategy({
    usernameField: 'phone',
    passwordField: 'password',
    passReqToCallback: true
}, function(req, phone, password, done) {
    req.checkBody('phone', 'wrong phone number').notEmpty();
    req.checkBody('password', 'wrong password').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        var messages = [];
        errors.forEach(function(error) {
            messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }
    User.findOne({'phone': phone}, function (err, user) {
        if (err) {
            return done(err);
        }
        if (!user) {
            return done(null, false, {message: 'No user found.'});
        }
        if (!user.validPassword(password)) {
            return done(null, false, {message: 'Wrong password.'});
        }
        return done(null, user);
    });
}));